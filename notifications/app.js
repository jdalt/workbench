var Notary = require('./notify');
var moment = require('moment');

var options = {
  body: "The body of the message does not include html text.",
  tag: 'wtf?',
  icon: 'icon.png'
};

var notifier = Notary.createNotifier(options);

function handleClick() {
  console.log("boom. done handled");
}

var intervalIndex;

UI.notifyMe = function() {
  notifier.getPermission();
  clearInterval(intervalIndex);
  console.log('put in the order');
  var timeStamp = moment();
  intervalIndex = setInterval(function() {
    var countup = moment() - timeStamp;
    console.log(countup);
    if(countup > 5000) {
      console.log('dispatch message');
      notifier.sendMessage('Victory', "Time of win: " + moment().seconds(), handleClick);
      clearInterval(intervalIndex);
    }
  }, 500);
}
