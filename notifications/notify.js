var options = {};
var hasPermission = false;
function createNotifier(opts) {
  options = opts; // need to copy?
  var supported = "Notification" in window;
  if (!supported) {
    return {
      sendMessage: function notSupportedSend() {
        console.log("This browser does not support desktop notifications.");
      }
    }
  }

  return {
    getPermission: getPermission,
    sendMessage: sendMessage
  }
}

// Checks current perms and requests them if they aren't granted or denied.
function getPermission() {
  if (Notification.permission === "granted") {
    hasPermission = true;
  } else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // Persist request info.
      if(!('permission' in Notification)) {
        Notification.permission = permission;
      }
      if (permission === "granted") {
        hasPermission = true;
      } 
    });
  }

}

// Checks permissions and sends if they're good.
function sendMessage(message, body, onclick) {
  if (hasPermission) {
    _sendMessage(message, body, onclick);
  } else {
    getPermission();
    if(hasPermission) {
      _sendMessage(message, body, onclick);
    }
  }
}

// Creates and sends the message.
function _sendMessage(message, body, onclick) {
  options.body = body;
  var notification = new Notification(message, options);
  notification.onclick = function(e) {
    console.log("onclick");
    onclick(e);
  };
  notification.onshow = function(e) {
    console.log("show");
  };
  notification.onclose = function(e) {
    console.log("close");
  };
  notification.onerror = function(e) {
    console.log("error");
  };
}

exports.createNotifier = createNotifier;
