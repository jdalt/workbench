#!/usr/bin/env zsh

# TODO more exclusions
# CONSIDER: how dangerous is eval?

script_name=$1
script_paths=($(find ~/workbench -type f | grep -v "\.swp" | grep $script_name ) )


#echo "${#script_paths[@]}" # weird syntax for array count. kept as reminder

if [ "${#script_paths[@]}" -gt 1 ]
then
  echo "'$script_name' resolved to multiple paths. Select one:"
  PS3="script:"
  select script_path in "${script_paths[@]}";
  do
    break
  done
else
  script_path=${script_paths[1]}
fi

shift
eval $script_path $@
