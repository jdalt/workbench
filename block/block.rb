def rock_out(stuff, &action)
  puts "Get ready to rock."
  puts stuff
  yield(6)
  puts ""
end

def rock_out2(stuff, &action)
  puts "rock_out 2"
  puts "Get ready to rock."
  puts stuff
  action.call(6)
  puts ""
end

spazz = Proc.new { |take| puts take }

def other_func(go)
  puts go
  puts go
  puts go
end

rock_out(5) { |t| other_func(t) }
rock_out(5, &spazz)
rock_out2(5) { |t| other_func(t) }
rock_out2(5, &spazz)
