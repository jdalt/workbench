#!/usr/bin/env zsh

relpath=$1

if [ -z "$relpath" ]
then
  relpath="."
fi

find $relpath -type f -not -path "*/node_modules/*" -not -path "*/.git/*" -exec ls -l {} \; | grep -v total | awk '{print $5, $9}' | sort -gr
