#!/usr/bin/env zsh

relpath=$1

if [ -z "$relpath" ]
then
  relpath="."
fi

dirs=($(find $relpath -type d -not -path "*/node_modules/*" -not -path "*/.git/*"))

for dir in "${dirs[@]}"
do
  echo "$(ls -l "$dir" | wc -l) $dir"
done

