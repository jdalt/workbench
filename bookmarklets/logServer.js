var express = require('express');
var app = new express();

app.get('/esp/:word', function(req,res) {
  var target = req.params.word;
  console.log(target);
  var forwardUrl = 'http://lema.rae.es/drae/?val=' + target;
  res.set('Access-Control-Allow-Origin', '*');
  res.send(forwardUrl);
});
app.listen(3001);

console.log('Cruising 3001');
