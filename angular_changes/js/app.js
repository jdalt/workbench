'use strict';

angular.module('changes', [
  'ui.router',
  'changes.home',
  'changes.directives'
])
.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('app', {
    abstract: true
  })
  ;

  $urlRouterProvider.otherwise('/home');
})

