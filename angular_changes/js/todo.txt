
// $scope.$watch takes two functions
      $scope.$watch(function() {
        var gsType = _.result($scope.gameSlot, 'scoring_type')
        var prefType = _.result($scope.statPreferences, 'scoring_type')
        return gsType === 'tabular' ||
          gsType === 'unscored' && prefType ==='tabular'
      }, function(scoreByInterval) {
        $scope.scoreByInterval = scoreByInterval
      })

      $scope.$watch('key', function callback() {
      })

      // Is equivlane to:
      $scope.$watch(function() {
         return $scope[key]
      },
        function callback() {
      })

