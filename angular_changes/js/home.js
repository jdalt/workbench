'use strict';

angular.module('changes.home', [
  'ui.router'
])
.config(function ($stateProvider) {
  $stateProvider
  .state('home', {
    url: '/home',
    templateUrl: 'js/home.tmpl.html',
    controller: 'HomeCtrl as home'
  })
})
.controller('HomeCtrl', function($scope) {

})
;

