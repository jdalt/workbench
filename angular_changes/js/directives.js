angular.module('changes.directives', [
  'ui.router'
])
.config(function ($stateProvider) {
  $stateProvider
  .state('directives', {
    url: '/directives',
    templateUrl: 'js/directives.tmpl.html',
    controller: 'DirectivesPageCtrl as dirCtrl'
  })
})
.controller('DirectivesPageCtrl', function($scope) {
  var dirCtrl = this;

  dirCtrl.dirCtrlProperty = "DirCtrlProp";

  dirCtrl.actOnParentProperty = function(paramName) {
    console.log('paramName', paramName);
    dirCtrl.dirCtrlProperty += " parent-controller-mutation!";
  }
})
.controller('DirectivesInnerCtrl', function($scope) {
  var dirInner = this;
  dirInner.actOnLocalProperty = function() {
    $scope.localProperty += " directive-mutation!";
  }

  dirInner.callLocalProperty = function() {
    $scope.localProperty({param: "directiveParam"});
  }
})
.directive('textScope', function() {

  return {
    controller: 'DirectivesInnerCtrl as tsInnerCtrl',
    restrict: 'E',
    scope: {
      localProperty: "@templateProperty"
    },
    template: '<h3>Text Scope Directive</h3>' +
              '<div ng-click="tsInnerCtrl.actOnLocalProperty()">Click Me for Directive Mutation</div>' +
              '<div>Local Property: {{localProperty}}</div>'
  }
})
.directive('twoWayScope', function() {

  return {
    controller: 'DirectivesInnerCtrl as twsInnerCtrl',
    restrict: 'E',
    scope: {
      localProperty: "=templateProperty"
    },
    template: '<h3>Two Way Scope</h3>' +
              '<div ng-click="twsInnerCtrl.actOnLocalProperty()">Click Me for Directive Mutation</div>' +
              '<div>Local Property: {{localProperty}}</div>' +
              '<label>Directive Input </label><input type="text" ng-model="localProperty" />'
  }
})
.directive('functionScope', function() {

  return {
    controller: 'DirectivesInnerCtrl as fsInnerCtrl',
    restrict: 'E',
    scope: {
      localProperty: "&templateProperty"
    },
    template: '<h3>Function Scope</h3>' +
              '<div ng-click="fsInnerCtrl.callLocalProperty()">Click Me for to call LocalProperty</div>'
  }
});

