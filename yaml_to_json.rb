require 'pry'

require 'json'
require 'yaml'
require 'fileutils'

base = 'db/meta_data/sports'
target = '~/sports'

# Don't monkey patch at home kids
class Hash
  def except!(*args)
    self.delete_if { |k,_| args.include?(k) }
  end
end


# Now for each sport
Dir.glob('db/meta_data/sports/*').each do |path|
  sport = path.split('/').last

  stat_modules = YAML.load_file("#{base}/#{sport}/stat_modules.yml")
  target_path = File.expand_path("#{target}/#{sport}")
  FileUtils.mkdir_p("#{target_path}/stats")
  FileUtils.mkdir_p("#{target_path}/standings")

  stat_modules.each do |key, stat_module|
    types = {}
    stat_module['stat_types'].each do |stat_type|
      stat_type['required_fields'] = stat_type['required_fields'].split(' ') if stat_type['required_fields']
      types[stat_type['key']] = stat_type.except!('_type', 'display_row', 'field_type', 'key')
    end

    stat_module['types'] = types
    stat_module['sport_id'] = stat_module['ngin_sport_id']
    stat_module['name'] = stat_module['short_name']

    stat_module.except!('ngin_sport_id', 'short_name', 'ngin_stat_module_id', 'scoring_module', '_type', 'save_priority', 'regulation_innings_required', 'stat_types')

    json_str = JSON.pretty_generate(stat_module)
    subfolder = 'stats'
    subfolder = 'standings' if key.include?('team_scoring')
    File.open("#{target_path}/#{subfolder}/#{stat_module['key']}.json", 'w') { |file| file.write(json_str) }
  end
end
