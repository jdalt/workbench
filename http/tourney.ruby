require 'rest_client'

# Local
# TOURNEY_ID = '53fc9810b9e2fc7edc000001'
# BASE_PATH = 'localhost:3001'

# Staging
TOURNEY_ID = '53d95777fd08ca4856000008'
BASE_PATH = 'api-stat-ngin.stage.ngin-staging.com'

ACCESS_TOKEN = 'ca89cd7fc5dec564a3a0c1199f451566'
headers = {
  'Accept' => 'application/vnd.stat-ngin.v2,application/json',
  'Authorization' => "Bearer #{ACCESS_TOKEN}",
  'Stat-Ngin-Api-Token' => 'b29bfb03d77b22646836451dbbe85d5bb5c4b932b149faeb4320d5c0b6e26dd2'
}

response = RestClient.get("#{BASE_PATH}/tournaments/#{TOURNEY_ID}", headers)
puts response.inspect
