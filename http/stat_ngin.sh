#!/usr/bin/env zsh

# Local
baseUrl="localhost:3001"
# tid=542dba4cb9e2fc1981000001 # Blastiopod
# team1=542dba9ab9e2fc1ed3000020 # Blood Reapers
# team2=542dba9ab9e2fc1ed300003e # War Slammers
# tid=54219dbab9e2fc20af000001
# team1=54219dd5b9e2fc23ce000022
# team2=54219dd5b9e2fc23ce000040
# fid=544038f7b9e2fc028d000073

# Staging
# baseUrl="http://api-stat-ngin.stage.ngin-staging.com/"
# tid=542d8835fd08ca1b7f000003
# team1=542d8f57fd08ca7d21000057 # Rando Bananas, unflighted (orig)
# team2=542d88ce53e3d3655a000035 # Road Warriors, flighted P
# team1=542d8f44fd08ca7d21000053 # Beats
# fid=54409dc1025a585ca4000001

bearer=$1

# swap teams
# curl -i -X PUT "${baseUrl}/tournaments/${tid}/swap_teams" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H 'Content-Type: application/json' -H "Authorization: Bearer ${bearer}" -d "{\"team_ids\": [\"${team1}\", \"${team2}\"]}"

# get teams
# curl -i -X GET "${baseUrl}/tournaments/${tid}/teams" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H "Authorization: Bearer ${bearer}"

# get teams from seasons endpoint
# sid=545959b4b9e2fc6857000002
# curl -i -X GET "${baseUrl}/seasons/${sid}/teams?has_completed_games=0" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H "Authorization: Bearer ${bearer}"

# create double elimination stage
# curl -i -X POST "${baseUrl}/flights/${fid}/flight_stages" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H 'Content-Type: application/json' -H "Authorization: Bearer ${bearer}" -d "{\"errors\": {},\"type\":\"double_elim\",\"name\":\"p\",\"full_elimination\":false,\"seeding_method\":\"traditional\",\"fixed_seeding\":true,\"flight_id\":\"${fid}\",\"position\":1,\"template\":\"double_elim\",\"advancement\":[], \"guarantee\":true}"

# sid=54219d5cb9e2fc1b87000002
# qry="${baseUrl}/team_stats?group_by[]=team_id&group_by[]=game_id&group_by[]=subseason_id&group_by[]=_id&scope[game_id][$in][]=1"
# qry="${baseUrl}/team_stats?group_by%6B%5D=team_id&group_by%5B%5D=game_id&group_by%5B%5D=subseason_id&group_by%5B%5D=_id&scope%5Bgame_id%5D%5B%24in%5D%5B%5D=4"
# qry="${baseUrl}/team_stats?game_id=1"
# qry="${baseUrl}/team_stats?game_id\[\]=545959e9b9e2fc6d9b000013&game_id\[\]=1"
# qry="${baseUrl}/team_stats/5421e1f8b9e2fc9dac000026"
# curl -i -X GET "${qry}" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H 'Content-Type: application/json' -H "Authorization: Bearer ${bearer}"

# qry="${baseUrl}/team_stats"
# curl -i -X POST "${qry}" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H 'Content-Type: application/json' -H "Authorization: Bearer ${bearer}" -d '{"game_id": "542ec45ab9e2fc285f000001", "team_id": "542ec42db9e2fc256100003e", "season_id": "54219dbab9e2fc20af000003", "values": {}}'
# curl -i -X POST "${qry}" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H 'Content-Type: application/json' -H "Authorization: Bearer ${bearer}" -d '{"game_id":"542ec45ab9e2fc285f000001", "season_id":"5473b97210a9aee52f000002", "team_id":"54219dd4b9e2fc23ce000011", "player_id":"5473b9a110a9aee52f000010", "stat_module_id":"54219d5db9e2fc1b87000163", "game_type":"tmt", "added":true, "values":{"hkssog":"4"}}'


# {"tournament_id":"54e26308b9e2fc73a1000001","filters":{"are":{"enabled":true,"fetched":true,"active":true},"flight_id":"54e2633db9e2fc73a1000005"},"status":"deleted"}
# {"tournament_id":"54e26308b9e2fc73a1000001", "filters":{"are":{"enabled":true, "fetched":true, "active":true}, "flight_id":"54e2633db9e2fc73a1000005"}, "status":"deleted", "query":{"tournament_id":"54e26308b9e2fc73a1000001"}}
# curl -i -X PUT "${baseUrl}/tournament_schedules" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H 'Content-Type: application/json' -H "Authorization: Bearer ${bearer}" -d '{"tournament_id":"54e26308b9e2fc73a1000001","filters":{"are":{"enabled":true,"fetched":true,"active":true},"flight_id":"54e2633db9e2fc73a1000005"},"status":"deleted"}'

# '{"flight_id":"54e2633db9e2fc73a1000005", "flight_stage_id":"54e2635eb9e2fc73a1000006", "flight_stage_position":1, "bracket_node_id":"54e2635eb9e2fc73a1000013", "bracket_id":"54e2635eb9e2fc73a1000008", "pool_ids":nil, "pool_id":nil, "round":nil, "dirty":false, "seeds":[1, 8], "game_id":"54e2635eb9e2fc73a1000012", "home_team_id":"54e26374b9e2fc76ff000020", "starts_at":nil, "ends_at":nil, "status":"deleted", "sport_id":1, "scores":{"54e26374b9e2fc76ff000020":nil, "54e26374b9e2fc76ff000032":nil}, "scoring_type":"unscored", "interval_scores":{}, "overtime":false, "shootout":nil, "bye":false, "tbd_time":true, "updated_at":"2015-02-17T12:02:12-06:00", "subvenue_id":nil, "subvenue_name":nil, "venue_id":nil, "venue_name":nil, "game_number":1, "flight_name":"Vertiginous", "flight_stage_name":"Dubious Eliminations", "pool_name":nil, "flight_short_name":"Vertiginous", "flight_abbrev":"VRTGNS", "flight_ages_supported":["A"], "team_advancing_id":nil, "conflicts":0, "teams":[{"name":"Blood Reapers", "short_name":"Blood Reapers", "abbrev":"BLDRPS", "placeholder_id":"54e2635eb9e2fc73a1000022", "team_id":"54e26374b9e2fc76ff000020", "seed":1}, {"name":"Rad Rockers", "short_name":"Rad Rockers", "abbrev":"RADRCS", "placeholder_id":"54e2635eb9e2fc73a1000024", "team_id":"54e26374b9e2fc76ff000032", "seed":8}], "flight_stage_abbrev":"DBSELS", "bracket_abbrev":"BR", "name":"Game 1 - Vertiginous - Dubious Eliminations", "abbrev":"G1 - VRTGNS - DBSELS", "num_officials":0, "cid":"c10", "starts_at_date":"", "starts_at_time":""}'
# {"id"=>"54e2635eb9e2fc73a100001d", "flight_id"=>"54e2633db9e2fc73a1000005", "flight_stage_id"=>"54e2635eb9e2fc73a1000006", "flight_stage_position"=>1, "bracket_node_id"=>"54e2635eb9e2fc73a1000013", "bracket_id"=>"54e2635eb9e2fc73a1000008", "pool_ids"=>nil, "pool_id"=>nil, "round"=>nil, "dirty"=>false, "seeds"=>[1, 8], "game_id"=>"54e2635eb9e2fc73a1000012", "home_team_id"=>"54e26374b9e2fc76ff000020", "starts_at"=>nil, "ends_at"=>nil, "status"=>"deleted", "sport_id"=>1, "scores"=>{"54e26374b9e2fc76ff000020"=>nil, "54e26374b9e2fc76ff000032"=>nil}, "scoring_type"=>"unscored", "interval_scores"=>{}, "overtime"=>false, "shootout"=>nil, "bye"=>false, "tbd_time"=>true, "updated_at"=>"2015-02-17T12:02:12-06:00", "subvenue_id"=>nil, "subvenue_name"=>nil, "venue_id"=>nil, "venue_name"=>nil, "game_number"=>1, "flight_name"=>"Vertiginous", "flight_stage_name"=>"Dubious Eliminations", "pool_name"=>nil, "flight_short_name"=>"Vertiginous", "flight_abbrev"=>"VRTGNS", "flight_ages_supported"=>["A"], "team_advancing_id"=>nil, "conflicts"=>0, "teams"=>[{"name"=>"Blood Reapers", "short_name"=>"Blood Reapers", "abbrev"=>"BLDRPS", "placeholder_id"=>"54e2635eb9e2fc73a1000022", "team_id"=>"54e26374b9e2fc76ff000020", "seed"=>1}, {"name"=>"Rad Rockers", "short_name"=>"Rad Rockers", "abbrev"=>"RADRCS", "placeholder_id"=>"54e2635eb9e2fc73a1000024", "team_id"=>"54e26374b9e2fc76ff000032", "seed"=>8}], "flight_stage_abbrev"=>"DBSELS", "bracket_abbrev"=>"BR", "name"=>"Game 1 - Vertiginous - Dubious Eliminations", "abbrev"=>"G1 - VRTGNS - DBSELS", "num_officials"=>0, "cid"=>"c10", "starts_at_date"=>"", "starts_at_time"=>""}
curl -i -X PUT "${baseUrl}/tournament_schedules/54e2635eb9e2fc73a100001d" -H "Accept: application/vnd.stat-ngin.v2,application/json" -H 'Content-Type: application/json' -H "Authorization: Bearer ${bearer}" -d '{"id":"54e2635eb9e2fc73a100001e","flight_id":"54e2633db9e2fc73a1000005","flight_stage_id":"54e2635eb9e2fc73a1000006","flight_stage_position":1,"bracket_node_id":"54e2635eb9e2fc73a1000015","bracket_id":"54e2635eb9e2fc73a1000008","pool_ids":[],"pool_id":null,"round":null,"dirty":false,"seeds":[4,5],"game_id":"54e2635eb9e2fc73a1000014","home_team_id":"54e26374b9e2fc76ff00002c","starts_at":null,"ends_at":null,"status":"scheduled","sport_id":1,"scores":{"54e26374b9e2fc76ff00002c":null,"54e26374b9e2fc76ff000014":null},"scoring_type":"unscored","interval_scores":{},"overtime":null,"shootout":null,"bye":false,"tbd_time":true,"updated_at":"2015-02-17T12:02:12-06:00","subvenue_id":null,"subvenue_name":null,"venue_id":null,"venue_name":null,"game_number":2,"flight_name":"Vertiginous","flight_stage_name":"Dubious Eliminations","pool_name":null,"flight_short_name":"Vertiginous","flight_abbrev":"VRTGNS","flight_ages_supported":["A"],"team_advancing_id":null,"conflicts":0,"teams":[{"name":"Icebay Bashers","short_name":"Icebay Bashers","abbrev":"ICYBSS","placeholder_id":"54e2635eb9e2fc73a1000026","team_id":"54e26374b9e2fc76ff00002c","seed":4},{"name":"Killer Konvicts","short_name":"Killer Konvicts","abbrev":"KLRKNS","placeholder_id":"54e2635eb9e2fc73a1000028","team_id":"54e26374b9e2fc76ff000014","seed":5}],"flight_stage_abbrev":"DBSELS","bracket_abbrev":"BR","name":"Game 2 - Vertiginous - Dubious Eliminations","abbrev":"G2 - VRTGNS - DBSELS","num_officials":0,"cid":"c12","starts_at_date":"","starts_at_time":""}'
