#!/usr/bin/env ruby

require('ostruct')

# Example Use:
# ruby 

DESC = "desc".freeze
ASC = "asc".freeze
class AbstractFilter
  def initialize(order)
    @order = order
  end

  def tie(a,b)
    raise NotImplementedError
  end

  def greater(a,b)
    raise NotImplementedError
  end

  def less(a,b)
    return greater(b,a)
  end

  def sort(list)
    list.sort! do |a, b|
      if desc
        next -1 if greater(a,b)
        next 0 if tie(a,b)
        next 1 if less(a,b) 
      else
        next -1 if less(a,b)
        next 0 if tie(a,b)
        next 1 if greater(a,b) 
      end
    end
  end

  def desc
    @order == DESC
  end
end

class FilterFactory < AbstractFilter
  def initialize(sort_var, order)
    @sort_var = sort_var
    super(order)
  end
  def tie(a,b)
    return true if a.send(@sort_var) == b.send(@sort_var)
    return false
  end

  def greater(a,b)
    return true if a.send(@sort_var) > b.send(@sort_var)
    return false
  end
end

class FilterRunner
  class << self
    def aggregate_ties(list, filter)
      count = 0
      last = nil
      tmp_array = []
      stacked_array = []
      while count < list.length
        current = list[count]
        if !current.nil? && !last.nil? && !filter.tie(current,last) && !tmp_array.empty?
          stacked_array << tmp_array 
          tmp_array = []
        end
        tmp_array << current
        last = current
        count += 1
      end
      stacked_array << tmp_array
      stacked_array
    end

    def run_filters(list, filterList)
      filter = filterList.shift
      list = [list] if list.first.class != Array # always an array of arrays
      list.map! do |sub_list|
        if sub_list.length > 1
          sub_list = filter.sort(sub_list)
          sub_list = aggregate_ties(sub_list, filter)
          run_filters(sub_list, filterList) if (filterList && filterList.length > 0) && (sub_list && sub_list.length > 1)
        end
        sub_list
      end
    end
  end
end

o_1_1_1     = OpenStruct.new({a:1, b:1, c:1})
o_1_1_10    = OpenStruct.new({a:1, b:1, c:10})
o_1_10_1    = OpenStruct.new({a:1, b:10, c:1})
o_1_10_10   = OpenStruct.new({a:1, b:10, c:10})
o_10_1_1    = OpenStruct.new({a:10, b:1, c:1})
o_10_1_10   = OpenStruct.new({a:10, b:1, c:10})
o_10_10_1   = OpenStruct.new({a:10, b:10, c:1})
o_10_10_10  = OpenStruct.new({a:10, b:10, c:10})

list = [
  o_1_1_1,
  o_1_1_10,
  o_1_10_1,
  o_1_10_10,
  o_10_1_1,
  o_10_1_10,
  o_10_10_1,
  o_10_10_10
]


filter_a = FilterFactory.new("a", ASC)
filter_b = FilterFactory.new("b", ASC)
filter_c = FilterFactory.new("c", ASC)


filters = [filter_a, filter_b, filter_c]

puts list.inspect
ranks = FilterRunner.run_filters(list, filters)
puts list.inspect
puts ranks.inspect

# puts "returned"
# puts ranks.inspect
# puts "flattened"
# puts ranks.flatten.inspect
# puts ranks.flatten.length
# puts ranks.flatten
