#!/usr/bin/env ruby

require 'csv'

# script example:
# ruby ~/workbench/csv_ruby/csv_join.rb unified_team_list_chsaa.csv season_list.csv season_id season_name > unified_team_list_chsaa_season_name.csv

def reindex_to_field(list, key)
  indexed_list = {}
  list.each do |item|
    key_value = item[key]
    indexed_list[key_value] = item
  end
  indexed_list
end

primary_file = ARGV[0]
join_file = ARGV[1]
join_key = ARGV[2]
join_target = ARGV[3]

io_primary = open(primary_file, "r:UTF-8").read
io_join = open(join_file, "r:UTF-8").read

primary_data = CSV.new(io_primary, { headers: true })
join_data = CSV.new(io_join, { headers: true })

join_hash = reindex_to_field(join_data, join_key)

puts join_target + "," + io_primary.lines.first # output the headers
primary_data.each do |line|
  join_key_value = line[join_key] # key value in primary data for the join_key
  puts join_hash[join_key_value][join_target] + "," + line.to_s
end
