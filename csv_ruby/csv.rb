#!/usr/bin/env ruby

require 'csv'

file_name = ARGV[0] || "#{ENV['HOME']}/workbench/csv_ruby/test_data.csv"
field = ARGV[1] || 'category'
file_io = open(file_name, "r:UTF-8").read

file_csv = CSV.new(file_io, { headers: true })

file_csv.each do |line|
  puts "#{field}: " + line[field] if line[field]
end
