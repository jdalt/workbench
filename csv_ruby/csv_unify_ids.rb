#!/usr/bin/env ruby

require 'csv'

# example use:
# ruby csv_unify_ids.rb scraped_data_league_714/team_list.csv team_name 10000 > unified_team_list_chsaa.csv

# add global addressing to csv in which $field is duplicated multiple times
file_name = ARGV[0] 
field = ARGV[1]
initial_index = ARGV[2] || 1 # start at 1 or the passed in value
file_io = open(file_name, "r:UTF-8").read

file_csv = CSV.new(file_io, { headers: true })

unified_hash = {}
file_csv.each do |line|
  field_key = line[field]
  unified_hash[field_key] = [] if unified_hash[field_key].nil?
  unified_hash[field_key] << line
end

puts "global_#{field}_index,#{file_io.lines.first}"
global_index = initial_index.to_i
unified_hash.each_value do |unified_array|
  unified_array.each do |individual_line|
    puts "#{global_index},#{individual_line}"
  end
  global_index += 1
end
