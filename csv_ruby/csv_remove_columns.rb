#!/usr/bin/env ruby

require 'csv'

primary_file = ARGV[0]
remove_columns = ARGV[1].split(" ")

io_primary = open(primary_file, "r:UTF-8").read
primary_data = CSV.new(io_primary, { headers: true })

original_headers = io_primary.lines.first.split(",")
original_headers.map! { |item| item[/\w*/] }

filtered_headers = original_headers - remove_columns # set substraction

puts filtered_headers.join(',')
primary_data.each do |line|
  output_data = []
  filtered_headers.each do |header|
    output_data << line[header]
  end
  puts output_data.join(',')
end
