#!/usr/bin/env ruby

require 'csv'

# Example Use:
# ruby ~/workbench/csv_ruby/csv_filter.rb unified_team_list_caha_season_name.csv master_roster/roster_seasons.csv season_id > unified_team_list_recent_caha.csv

primary_file = ARGV[0]
filter_file = ARGV[1]
filter_key = ARGV[2]

io_primary = open(primary_file, "r:UTF-8").read
io_filter = open(filter_file, "r:UTF-8").read

primary_data = CSV.new(io_primary, { headers: true })
filter_data = CSV.new(io_filter, { headers: true })

filter_list = []
filter_data.each do |line|
  filter_list << line[filter_key]
end

puts io_primary.lines.first # output the headers
primary_data.each do |line|
  puts line if filter_list.include?(line[filter_key])
end
