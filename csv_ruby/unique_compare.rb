#!/usr/bin/env ruby

f1n = ARGV[0]
f2n = ARGV[1]

f1 = File.readlines(f1n).map(&:to_i).uniq
f2 = File.readlines(f2n).map(&:to_i).uniq

puts "#{f1n} has #{f1.length} unique items"
puts "#{f2n} has #{f2.length} unique items"
puts

uniq_diff = f1 - f2

puts "#{f1n} has #{uniq_diff.length} exclusive items"
puts "Diff: "
puts uniq_diff.inspect
puts ""

uniq_intersection = f1 & f2
puts "#{f1n} and #{f2n} has #{uniq_intersection.length} intersected items"
puts "Intersection: "
puts uniq_intersection.inspect
