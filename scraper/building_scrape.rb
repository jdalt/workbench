#!/usr/bin/env ruby

require File.expand_path(File.join(File.dirname(__FILE__), 'scraping_utils.rb'))


scraper = ScrapingUtils::BasicScraper.new('http://en.wikipedia.org/wiki/List_of_tallest_buildings_in_Minneapolis')

urls = scraper.doc.xpath("//table[@class='wikitable sortable']//td[@align='left']/a").to_a.map do |href|
  str = href.attribute('href').value
  next str if str.include?('wiki')
  nil
end.compact

puts urls.inspect

