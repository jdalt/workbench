#!/usr/bin/env ruby
#encoding: UTF-8

# Nick R (nick.rndl@gmail.com)

require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'pry'

module ScrapingUtils

  module NokoParser

    attr_accessor :doc

    def build_doc(body)
      @doc = Nokogiri::HTML(body)
    end

    def extract_results
      result_variables.map {|name|
        self.instance_eval(name)
      }
    end

    def extract_results_with_keys
      result_variables.map {|name|
        [name, self.instance_eval(name)]
      }
    end

    def result_variables
      []
    end

   # todo
    def extract_strings(query, d=nil)
      extract_string(query, d, "\x00").split("\x00")
    end

    def extract_string(query, d=nil, sep=nil)
      doc = d || @doc
      separator = sep || ''
      substrings = []
      found = doc.xpath(query)
      if found.kind_of? Enumerable
        found.each do |el|
          substrings << normalize_substring(el.content)
        end
      else
        substrings << found.content
      end
      normalize_string(substrings.join(separator))
    end

    def normalize_string str
      str
    end

    def normalize_substring str
      normalize_string(str)
    end
  end


  module BasicFetcher
    def fetch url, use_cache=true
      begin
        str = open(url){|f| f.read }
      rescue => e
        puts "error:fetch: #{url}"
        p e
        puts e.backtrace
      end
    end
  end


  class BasicScraper
    include NokoParser
    include BasicFetcher

    attr_reader :url, :use_cache

    def initialize(url)
      @url = url
      build_doc(body)
    end

    def body
      @body ||= fetch(url)
    end

    def uri
      @uri ||= URI.parse(url)
    end
  end
end
