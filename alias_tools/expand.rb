#!/usr/bin/env ruby

require 'pry'

def get_alias(aliases, key)
  content = aliases[key]
  commands = content.split(" ")
  commands.map! do |cm|  #map?
    if aliases.keys.include?(cm) && cm != key
      get_alias(aliases, cm) 
    else
      cm
    end
  end
  commands.join(" ")
end


aliases = {}

lines = File.readlines('alias_list.txt').map{|m| m.strip }

lines.each do |line|
  alias_key = line[/([^=]*)=/,1].tr("'\"","")
  alias_content = line[/[^=]*=(.*)/,1].tr("'\"","")
  aliases[alias_key] = alias_content
end

def sub_step(aliases, key)
  content = aliases[key]
  return if content.nil?
  commands = content.split(" ")
  commands.map! do |cm|  #map?
    if aliases.keys.include?(cm) && cm != key
      get_alias(aliases, cm)
    else
      cm
    end
  end
  commands.join(" ")
end

dealiased = {}
aliases.keys.each do |key|
  dealiased[key] = sub_step(aliases, key)
end

dealiased.keys.each do |key|
  puts "#{key} \t\t #{dealiased[key]}"
end

